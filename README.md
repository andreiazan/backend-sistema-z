# Avaliação

### Problema

Um Sistema Z enviará para nós uma requisição POST com o status do sistema no formato JSON a cada alteração interna. Precisamos armazenar esta informação em um banco de dados SQL Server em duas tabelas por meio de Store Procedure.

### Desafio

O candidato precisa criar:

1. uma solução em .NET 5+ (ou .NetCore 3.1) para receber a requisição do Sistema Z e enviar para a Store Procedure no SQL Server
2. criar a Store Procedure que fará duas tarefas:
    1. atualiza o status do Sistema Z na tabela "controle"
    2. insere o JSON recebido na tabela "log"

Pontos de avaliação do candidato:

* Registro por meio de git commit das etapas de produção dos códigos
* Organização dos códigos e documentação
* Manual de como preparar para executar o código

### Dados iniciais

O sistema Z enviará um POST para uma URL (para testes locais algo como http://127.0.0.1/api) com o seguinte formato JSON:

```
{
	'date': '2021-05-18 10:00:00',
	'status': 'Sistema em funcionamento'
}
```

Outro exemplo

```
{
	'date': '2021-05-18 10:00:00',
	'status': 'Sistema parado',
	'cause': 'Reach max users requests'
}
```

Dica: simule o envio do JSON usando o programa Postman ou similar. A resposta da requisição deve ser um HTTP 200 OK quando sucesso e HTTP 400 caso o JSON esteja fora do padrão. 

Para o SQL Server inicie um database com o seguinte código:

```
CREATE TABLE controle (
    id varchar(50),
    status varchar(255),
    date datetime
);
INSERT INTO controle (id, status, date) VALUES ('Sistema Z', '', '2021-05-18 00:00:00');
```

### Entrega

Primeiramente, você deve fazer um fork deste repositório, e versionar o seu código nele; Para isto é necessário ter cadastro no www.gitlab.com.

Forneça as instruções em um arquivo INSTRUCOES.md sobre como montar a sua aplicação, mencionando cada etapa necessária para que consigamos colocá-la para rodar. Especifique qual banco de dados você usou, e qual versão dos recursos utilizados.

Quando terminar, você deve entregar o código fazendo um merge request. Atenção: uma vez submetido o request, o seu teste estará finalizado e não será mais possível voltar atrás.

Boa sorte!
